## 0.2.0

### Added

    * Functionality to use different namespaces for influxDB was added by @neurocis
    * Ping monitoring improved by @neurocis. It can now monitor on a much more frequent interval
    * Can now choose a specific server to test from using `SERVER_ID`
    * 

### Changed

    * Improved logging
    * Imrpoved handling of data types. 

### Removed

    * Ability to run script manually was removed.   